package com.example.pre_examen_kotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var lblNombreTrabajador: TextView
    private lateinit var txtNombreTrabajador: EditText

    private lateinit var btnEntrar: Button
    private lateinit var btnSalir: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        btnEntrar.setOnClickListener {
            entrar()
        }

        btnSalir.setOnClickListener {
            salir()
        }
    }

    private fun iniciarComponentes() {
        lblNombreTrabajador = findViewById(R.id.lblNombreTrabajador)
        txtNombreTrabajador = findViewById(R.id.txtNombreTrabajador)
        btnEntrar = findViewById(R.id.btnEntrar)
        btnSalir = findViewById(R.id.btnSalir)
    }

    private fun entrar() {
        val strNombre = applicationContext.resources.getString(R.string.nombre)

        if (strNombre == txtNombreTrabajador.text.toString().trim()) {
            // Hacer el paquete para enviar información
            val bundle = Bundle()
            bundle.putString("nombre", txtNombreTrabajador.text.toString())

            // Crear el intent para llamar a otra actividad
            val intent = Intent(this@MainActivity, ReciboNominaActivity::class.java)
            intent.putExtras(bundle)

            // Iniciar la actividad esperando o no respuesta
            startActivity(intent)
        } else {
            Toast.makeText(applicationContext, "El usuario o contraseña no es válido", Toast.LENGTH_SHORT).show()
        }
    }

    // Finalizar
    private fun salir() {
        finish()
    }

}

