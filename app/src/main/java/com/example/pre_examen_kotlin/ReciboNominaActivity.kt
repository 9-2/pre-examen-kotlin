package com.example.pre_examen_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class ReciboNominaActivity : AppCompatActivity() {
    private lateinit var txtNumRecibo: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtHorasTrabajadasNormales: EditText
    private lateinit var txtHorasTrabajadasExtra: EditText
    private lateinit var rbAuxiliar: RadioButton
    private lateinit var rbAlbañil: RadioButton
    private lateinit var rbIngObra: RadioButton

    private lateinit var lblUsuario: TextView
    private lateinit var txtSubtotal: EditText
    private lateinit var txtImpuesto: EditText
    private lateinit var txtTotalPagar: EditText

    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button

    private lateinit var recibo: ReciboNomina

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recibo_nomina)
        iniciarComponentes()

        val datos = intent.extras
        val nombre = datos?.getString("nombre")
        lblUsuario.text = nombre

        btnCalcular.setOnClickListener {
            calcular()
        }

        btnLimpiar.setOnClickListener {
            limpiar()
        }

        btnRegresar.setOnClickListener {
            regresar()
        }
    }

    private fun iniciarComponentes() {
        txtNumRecibo = findViewById(R.id.txtNumRecibo)
        txtNombre = findViewById(R.id.txtNombre)
        txtHorasTrabajadasNormales = findViewById(R.id.txtHorasTrabajadasNormales)
        txtHorasTrabajadasExtra = findViewById(R.id.txtHorasTrabajadasExtra)
        txtSubtotal = findViewById(R.id.txtSubtotal)
        txtImpuesto = findViewById(R.id.txtImpuesto)
        txtTotalPagar = findViewById(R.id.txtTotalPagar)

        lblUsuario = findViewById(R.id.lblUsuario)

        rbAuxiliar = findViewById(R.id.rbAuxiliar)
        rbAlbañil = findViewById(R.id.rbAlbañil)
        rbIngObra = findViewById(R.id.rbIngObra)

        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)

        recibo = ReciboNomina(0, "", 0f, 0f, 0, 0f)

        deshabilitarTextos()
    }

    private fun calcular() {
        var puesto = 1
        var horasNormales = 0f
        var horasExtras = 0f
        var subtotal = 0f
        var impuesto = 0f
        var total = 0f

        if (rbAuxiliar.isChecked) {
            puesto = 1
        } else if (rbAlbañil.isChecked) {
            puesto = 2
        } else if (rbIngObra.isChecked) {
            puesto = 3
        }

        if (txtNumRecibo.text.toString().isEmpty()) {
            Toast.makeText(this, "Por favor, ingrese su Número de Recibo", Toast.LENGTH_SHORT).show()
        } else if (txtNombre.text.toString().isEmpty()) {
            Toast.makeText(this, "Por favor, ingrese su Nombre", Toast.LENGTH_SHORT).show()
        } else if (txtHorasTrabajadasNormales.text.toString()
                .isEmpty() || txtHorasTrabajadasExtra.text.toString().isEmpty()
        ) {
            if (txtHorasTrabajadasNormales.text.toString().isEmpty()) {
                Toast.makeText(this, "Ingrese la Cantidad de horas Trabajadas", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Ingrese la Cantidad de Horas Extras", Toast.LENGTH_SHORT).show()
            }
        }

        if (!txtHorasTrabajadasNormales.text.toString().isEmpty()) {
            horasNormales = txtHorasTrabajadasNormales.text.toString().toFloat()
        }

        if (!txtHorasTrabajadasExtra.text.toString().isEmpty()) {
            horasExtras = txtHorasTrabajadasExtra.text.toString().toFloat()
        }

        if (!txtHorasTrabajadasNormales.text.toString()
                .isEmpty() && !txtHorasTrabajadasExtra.text.toString().isEmpty() && puesto != 0
        ) {
            subtotal = recibo.calcularSubtotal(puesto, horasNormales, horasExtras)
            impuesto = recibo.calcularImpuesto(puesto, horasNormales, horasExtras)
            total = recibo.calcularTotal(subtotal, impuesto)
            txtSubtotal.setText(subtotal.toString())
            txtImpuesto.setText(impuesto.toString())
            txtTotalPagar.setText(total.toString())
        }
    }

    private fun limpiar() {
        txtNumRecibo.text.clear()
        txtNombre.text.clear()
        txtHorasTrabajadasNormales.text.clear()
        txtHorasTrabajadasExtra.text.clear()
        txtSubtotal.text.clear()
        txtImpuesto.text.clear()
        txtTotalPagar.text.clear()
    }

    private fun regresar() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Recibo de Nomina")
        confirmar.setMessage("Regresar al MainActivity")
        confirmar.setPositiveButton("Confirmar") { dialog, which ->
            finish()
        }
        confirmar.setNegativeButton("Cancelar") { dialog, which ->
            // No hace nada
        }
        confirmar.show()
    }

    private fun deshabilitarTextos() {
        txtSubtotal.isFocusable = false
        txtImpuesto.isFocusable = false
        txtTotalPagar.isFocusable = false
    }
}
