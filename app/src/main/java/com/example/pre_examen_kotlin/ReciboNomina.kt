package com.example.pre_examen_kotlin

class ReciboNomina(private var numRecibo: Int, private var nombre: String, private var horasTrabNormal: Float, private var horasTrabExtras: Float, private var puesto: Int, private var impuestoPorc: Float) {

    fun setNumRecibo(numRecibo: Int) {
        this.numRecibo = numRecibo
    }

    fun getNumRecibo(): Int {
        return this.numRecibo
    }

    fun setNombre(nombre: String) {
        this.nombre = nombre
    }

    fun getNombre(): String {
        return this.nombre
    }

    fun setHorasTrabNormal(horasTrabNormal: Float) {
        this.horasTrabNormal = horasTrabNormal
    }

    fun getHorasTrabNormal(): Float {
        return this.horasTrabNormal
    }

    fun setHorasTrabExtras(horasTrabExtras: Float) {
        this.horasTrabExtras = horasTrabExtras
    }

    fun getHorasTrabExtras(): Float {
        return this.horasTrabExtras
    }

    fun setPuesto(puesto: Int) {
        this.puesto = puesto
    }

    fun getPuesto(): Int {
        return this.puesto
    }

    fun setImpuestoPorc(impuestoPorc: Float) {
        this.impuestoPorc = impuestoPorc
    }

    fun getImpuestoPorc(): Float {
        return this.impuestoPorc
    }

    fun calcularSubtotal(puesto: Int, horasTrabNormal: Float, horasTrabExtras: Float): Float {
        var pagoBase = 200f
        var total = 0f

        if (puesto == 1) {
            pagoBase += pagoBase * 0.20f
        } else if (puesto == 2) {
            pagoBase += pagoBase * 0.50f
        } else if (puesto == 3) {
            pagoBase += pagoBase * 1.00f
        } else {
            println("No se puede calcular")
        }

        total = pagoBase * horasTrabNormal + horasTrabExtras * pagoBase * 2

        return total
    }

    fun calcularImpuesto(puesto: Int, horasTrabNormal: Float, horasTrabExtras: Float): Float {
        val impuesto = calcularSubtotal(puesto, horasTrabNormal, horasTrabExtras) * 0.16f
        return impuesto
    }

    fun calcularTotal(subtotal: Float, impuesto: Float): Float {
        val totalPagar = subtotal - impuesto
        return totalPagar
    }
}
